Function New-CobaltServiceProvider {
param(
    [Parameter(HelpMessage="The URL for the OData endpoint")][string]$Url = (Get-DefaultCobaltUrl),
    [Parameter(HelpMessage="The credential to authenticate the OData call with")][PSCredential]$Credential = (Get-DefaultCobaltCredential),
    [Parameter(HelpMessage="The OData type of the new entity")][string]$ODataType = 'cobalt.CobaltServiceProvider',
    [Parameter(Mandatory=$True, HelpMessage="The name of the service provider")][string]$Name,
    [Parameter(HelpMessage="The description of the service provider")][string[]]$Description = @(),
    [Parameter(HelpMessage="The UUID of the container entity that will become the parent of the new service provider entity")][System.Guid]$ParentUUID = (Get-CobaltConfigurationUUID),
    [Parameter(Mandatory=$True, HelpMessage="The callback URL for the application")][string[]]$Callback,
    [Parameter(HelpMessage="The encryption algorithm to use")][string]$Algorithm = 'HS256',
    [Parameter(HelpMessage="The application secret")][string]$ClientSecret = (New-Guid).Guid,
    [Parameter(HelpMessage="An array of OAuth scopes for which user consent is assumed")][string[]]$ConsentAssumed = @(),
    [Parameter(HelpMessage="The lifetime in seconds for any security token issued")][int]$Duration = 3600,
    [Parameter(HelpMessage="Don't know")][string[]]$Entity = @(),
    [Parameter(HelpMessage="A JSON-encoded string describing the SAML attribute assertions to be provided with the authentication message")][string[]]$SamlAttributeConsumingService=@(),
    [Parameter(HelpMessage="A flag indicating that the IdP should use the SHA1 hashing algorithm")][Boolean]$UseSHA1 = $False,
    [Parameter(HelpMessage="The name of the external authentication service for Cobalt to use")][string]$AuthMode = "simple",
    [Parameter(HelpMessage="A flag indicating that the IdP should return errors to the application")][Boolean]$ReturnError = $False
)
    $properties = @{
        'Name'=$Name;
        'Description'=$Description;
        'Callback'=$Callback;
        'Algorithm'=$Algorithm;
        'ClientSecret'=$ClientSecret;
        'ConsentAssumed'=$ConsentAssumed;
        'Duration'=$Duration;
        'Entity'=$Entity;
        'SAMLAttributeConsumingService'=$SAMLAttributeConsumingService;
        'UseSHA1'=$UseSHA1;
        'ReturnError'=$ReturnError;
		'AuthMode'=$AuthMode;
		'userCertificate'=@()
    }
	# AuthMode can't be an empty string; it has to be either $null or a non-zero length string
	# if($AuthMode -eq "") {
	#	$properties['AuthMode'] = $null
	# }
    New-CobaltEntity -Url $Url -Credential $Credential -Properties $properties -ODataType $ODataType -ParentUUID $ParentUUID -Verbose:([bool]$PSBoundParameters['Verbose'].IsPresent)
}



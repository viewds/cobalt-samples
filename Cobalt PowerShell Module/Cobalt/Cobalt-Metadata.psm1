Function Add-CobaltMetadataProperty {
[CmdletBinding()]
Param(
    [Parameter(HelpMessage="The URL for the OData endpoint")][string]$Url = (Get-DefaultCobaltUrl),
    [Parameter(HelpMessage="The credential to authenticate the OData call with")][PSCredential]$Credential = (Get-DefaultCobaltCredential),
	[Parameter(HelpMessage="The OData primitive type of the property, e.g. Edm.String")][string]$DataType = 'Edm.String',
	[Parameter(HelpMessage="The default value of the property")][string]$DefaultValue = $null,
	[Parameter(HelpMessage="A switch indicating that the property is nullable")][switch]$Nullable,
	[Parameter(HelpMessage="A switch indicating that the property is a collection")][switch]$IsCollection,
	[Parameter(HelpMessage="An array of strings representing the facets of the new property")][string[]]$Facets=@()
)
	$Properties = New-CobaltMetadataProperty -DataType $DataType -DefaultValue $DefaultValue -Nullable:([bool]$PSBoundParameters['Nullable'].IsPresent) -IsCollection:([bool]$PSBoundParameters['IsCollection'].IsPresent) -Facets $Facets
	Write-Verbose ($Properties | ConvertTo-Json -Depth 3)
	$Properties | ConvertTo-Json -Depth 3 | Invoke-RestMethod -Uri "$(Get-DefaultCobaltUrl)/`$metadata/Properties" -Credential $(Get-DefaultCobaltCredential) -Method Post -Headers @{'Authorization'=$(Convert-CredentialToAuthHeader -Credential $(Get-DefaultCobaltCredential))} -Verbose:([bool]$PSBoundParameters['Verbose'].IsPresent)
}

Function New-CobaltMetadataProperty {
[CmdletBinding()]
Param(
	[Parameter(Mandatory=$true, HelpMessage="The fully namespace-qualified name of the entity type this property is part of")][string]$ODataType,
	[Parameter(Mandatory=$true, HelpMessage="The name of the property")][string]$Name,
	[Parameter(HelpMessage="The OData primitive type of the property, e.g. Edm.String")][string]$DataType = 'Edm.String',
	[Parameter(HelpMessage="The default value of the property")][string]$DefaultValue = $null,
	[Parameter(HelpMessage="A switch indicating that the property is nullable")][switch]$Nullable,
	[Parameter(HelpMessage="A switch indicating that the property is a collection")][switch]$IsCollection,
	[Parameter(HelpMessage="An array of strings representing the facets of the new property")][string[]]$Facets=@()
)
	# Return a hash table of the new property definition
	@{
		'@odata.type'='#Edm.Metadata.Property';
        'Fullname'="$ODataType/$Name";
        'Name'=$Name;
        'Nullable'=[boolean]$Nullable;
		'IsCollection'=[boolean]$IsCollection;
        'Type@odata.bind'="Types('$DataType')";
		'Facets'=$Facets
	}
}

Function New-CobaltMetadataType {
[CmdletBinding()]
Param(
    [Parameter(HelpMessage="The URL for the OData endpoint")][string]$Url = (Get-DefaultCobaltUrl),
    [Parameter(HelpMessage="The credential to authenticate the OData call with")][PSCredential]$Credential = (Get-DefaultCobaltCredential),
	[Parameter(HelpMessage="The namespace-qualified name of the new entity type", Mandatory=$True)][string]$QualifiedName,
	[Parameter(HelpMessage="The short name of the new entity type")][string]$ShortName = $QualifiedName.Split('.')[-1],
	[Parameter(HelpMessage="The fully qualified name of the entity type the new type inherits from")][string]$BaseType = '',
	[Parameter(HelpMessage="An array of CobaltMetadataProperty objects to that make up the new entity type")][array]$Properties = @(),
	[Parameter(HelpMessage="Switch indicating that the entity type is abstract")][Switch]$Abstract,
	[Parameter(HelpMessage="Switch indicating that the entity type is open")][Switch]$OpenType,
	[Parameter(HelpMessage="Switch indicating that the entity type has a string")][Switch]$HasStream
)
    #if([string]::IsNullOrEmpty($BaseType)) {
#		if($Abstract) {
#			$BaseType = 'com.viewds.cobalt.topAbstractType'
#		}
#		else {
#			$BaseType = 'com.viewds.cobalt.superiorType'
#		}
#	}

	$TypeProperties = @{
		'@odata.type'='#Edm.Metadata.EntityType';
		'QualifiedName'=$QualifiedName;
		'Name'=$ShortName;
		'Properties'=$Properties;
		'Abstract'=[boolean]$Abstract;
		'OpenType'=[boolean]$OpenType;
		'HasStream'=[boolean]$HasStream;
	}

	if(-not [string]::IsNullOrEmpty($BaseType)){
		$TypeProperties.Add('BaseType@odata.bind', $BaseType)
	}

	$TypeProperties | ConvertTo-Json -Depth 3 | Invoke-RestMethod -Uri "$Url/`$metadata/Types" -Credential $(Get-DefaultCobaltCredential) -Method Post -Headers @{'Authorization'=$(Convert-CredentialToAuthHeader -Credential $(Get-DefaultCobaltCredential))} -Verbose:([bool]$PSBoundParameters['Verbose'].IsPresent)
}

Function Get-CobaltMetadataTypes {
[CmdletBinding()]
Param(
    [Parameter(HelpMessage="The URL for the OData endpoint")][string]$Url = (Get-DefaultCobaltUrl),
    [Parameter(HelpMessage="The credential to authenticate the OData call with")][PSCredential]$Credential = (Get-DefaultCobaltCredential)
)
    (Get-CobaltQuery -Url $Url -Credential $Credential -Path '$metadata/Types' -Verbose:([bool]$PSBoundParameters['Verbose'].IsPresent)).value
}

Function Get-CobaltMetadataProperties {
[CmdletBinding()]
Param(
    [Parameter(HelpMessage="The URL for the OData endpoint")][string]$Url = (Get-DefaultCobaltUrl),
    [Parameter(HelpMessage="The credential to authenticate the OData call with")][PSCredential]$Credential = (Get-DefaultCobaltCredential)
)
    (Get-CobaltQuery -Url $Url -Credential $Credential -Path '$metadata/Properties' -Verbose:([bool]$PSBoundParameters['Verbose'].IsPresent)).value
}

Function Get-CobaltMetadataType {
[CmdletBinding()]
Param(
    [Parameter(HelpMessage="The URL for the OData endpoint")][string]$Url = (Get-DefaultCobaltUrl),
    [Parameter(HelpMessage="The credential to authenticate the OData call with")][PSCredential]$Credential = (Get-DefaultCobaltCredential),
	[Parameter(Position=0, Mandatory=$True, HelpMessage="The name of the entity type to return")][string]$ODataType
)
	$Path = "`$metadata/Types(`'$ODataType`')/Edm.Metadata.EntityType?`$expand=DerivedTypes,BaseType,Properties"
    Get-CobaltQuery -Url $Url -Credential $Credential -Path $Path -Verbose:([bool]$PSBoundParameters['Verbose'].IsPresent)
}

Function Get-CobaltMetadataFunctions {
[CmdletBinding()]
Param(
    [Parameter(HelpMessage="The URL for the OData endpoint")][string]$Url = (Get-DefaultCobaltUrl),
    [Parameter(HelpMessage="The credential to authenticate the OData call with")][PSCredential]$Credential = (Get-DefaultCobaltCredential)
)
    (Get-CobaltQuery -Url $Url -Credential $Credential -Path '$metadata/Functions' -Verbose:([bool]$PSBoundParameters['Verbose'].IsPresent)).value
}

# Function New-CobaltMetadataFunction {
# {
# Param(
#     [Parameter(HelpMessage="The URL for the OData endpoint")][string]$Url = (Get-DefaultCobaltUrl),
#     [Parameter(HelpMessage="The credential to authenticate the OData call with")][PSCredential]$Credential = (Get-DefaultCobaltCredential),
#     [Parameter(Mandatory=$True, HelpMessage="The fully qualified name of the function")][string]$QualifiedName,
#     [Parameter(Mandatory=$True, HelpMessage="The short name of the function")][string]$ShortName) = $QualifiedName.Split('.')[-1],
# 
# )
# $function = @{
	# '@odata.type'="#Edm.Metadata.Function";
# 	'QualifiedName=$QualifiedName';
	# 'Name'=$ShortName;
# 	'Overloads = @(
# #Array of IsBound, IsComposable, ReturnType { Nullable, Facets @(), IsCollection, Type { '@odata.id'= com.'}}, EntitySetPath, 
# 		'
# 	}
# 
	#POST to $/metadata/Functions
# "Overloads" : [ {
# "IsBound" : false,
# "IsComposable" : true,
# "ReturnType" : {
# "Nullable" : false,
# "Facets" : [ ],
# "IsCollection" : true,
# "Type" :
# { "@odata.id" : "Types('com.namescape.csp.User')" }
# },
# "EntitySetPath" : null,
# "Parameters" : [ ],
# "Annotations" : [ ]
# }, {
# "IsBound" : false,
# "IsComposable" : true,
# "ReturnType" : {
# "Nullable" : true,
# "Facets" : [ ],
# "IsCollection" : false,
# "Type" :
# { "@odata.id" : "Types('com.namescape.csp.User')" }
# # },
# "EntitySetPath" : null,
# "Parameters" : [ {
# "Name" : "Username",
# "IsBinding" : false,
# "Nullable" : false,
# "Facets" : [ ],
# "IsCollection" : false,
# "Annotations" : [ ],
# "Type" :
# { "@odata.id" : "Types('Edm.String')" }
# } ],
# "Annotations" : [ ]
# }, {
# "IsBound" : false,
# "IsComposable" : true,
# "ReturnType" : {
# "Nullable" : true,
# "Facets" : [ ],
# "IsCollection" : false,
# "Type" :
# { "@odata.id" : "Types('com.namescape.csp.User')" }
# },
# "EntitySetPath" : null,
# "Parameters" : [ {
# "Name" : "Email",
# "IsBinding" : false,
# "Nullable" : false,
# "Facets" : [ ],
# "IsCollection" : false,
# "Annotations" : [ ],
# "Type" :
# { "@odata.id" : "Types('Edm.String')" }
# } ],
# "Annotations" : [ ]
# }, {
# "IsBound" : false,
# "IsComposable" : true,
# "ReturnType" : {
# # # "Nullable" : true,
# "Facets" : [ ],
# # "IsCollection" : false,
# "Type" :
# { "@odata.id" : "Types('com.namescape.csp.User')" }
# },
# "EntitySetPath" : null,
# # "Parameters" : [ {
# "Name" : "DisplayName",
# "IsBinding" : false,
# "Nullable" : false,
# "Facets" : [ ],
# "IsCollection" : false,
# "Annotations" : [ ],
# # "Type" :
# { "@odata.id" : "Types('Edm.String')" }
# } ],
# "Annotations" : [ ]
# # }, {
# "IsBound" : false,
# "IsComposable" : true,
# "ReturnType" : {
# "Nullable" : true,
# "Facets" : [ ],
# "IsCollection" : false,
# "Type" :
# { "@odata.id" : "Types('com.namescape.csp.User')" }
# },
# # "EntitySetPath" : null,
# "Parameters" : [ {
# "Name" : "Surname",
# "IsBinding" : false,
# "Nullable" : false,
# # "Facets" : [ ],
# "IsCollection" : false,
# "Annotations" : [ ],
# "Type" :
# { "@odata.id" : "Types('Edm.String')" }
# }, {
# "Name" : "GivenName",
# "IsBinding" : false,
# "Nullable" : false,
# "Facets" : [ ],
# "IsCollection" : false,
# "Annotations" : [ ],
# "Type" :
# { "@odata.id" : "Types('Edm.String')" }
# # } ],
# "Annotations" : [ ]
# } ],
# "FunctionImports" : [ ]
# }
# }
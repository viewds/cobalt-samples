[System.Guid]$script:nullGUID = New-Object System.Guid 

Function Set-DefaultCobaltConnection {
[CmdletBinding()]
param (
    [Parameter(ParameterSetName="Command", Mandatory=$True, HelpMessage="The OData URL to use by default to connect to Cobalt if no URL is specified")][string]$Url,
    [Parameter(ParameterSetName="Command", HelpMessage="The credentials to use by default to connect to Cobalt if no credentials are specified")][PSCredential]$Credential,
    [Parameter(ParameterSetName="Command", HelpMessage="Flag indicating whether to check SSL certificates when connecting to OData service, `$True = check certs, `$False = ignore untrusted certs")][Switch]$CheckSSL,
	[Parameter(ParameterSetName="File", Mandatory=$True, HelpMessage="The name of the file containing the previously saved connection information")][string]$FilePath
)
	if($PsCmdlet.ParameterSetName -eq "Command") {
		# Save the specified URL and credentials for use by other cmdlets
		Initialize-DefaultConnection
		if($Url.EndsWith('/')) { # Remove trailing slash if present
			$Url = $Url.Remove($Url.Length - 1)
		}
		$global:_DefaultCobaltConnection.Url = $Url

		$global:_DefaultCobaltConnection.UserName = $Credential.UserName
		$global:_DefaultCobaltConnection.Password = $Credential.Password
		$global:_DefaultCobaltConnection.CheckSSL = $CheckSSL
		# Don't have to save this as it is global to the PoSh session
	}
	else {
		$global:_DefaultCobaltConnection = (Get-Content -Path $FilePath) | ConvertFrom-Json
		$global:_DefaultCobaltConnection.Password = ($global:_DefaultCobaltConnection.Password | ConvertTo-SecureString)
	}
	Write-Verbose "Url: $($global:_DefaultCobaltConnection.Url)"
	Write-Verbose "Username: $($global:_DefaultCobaltConnection.UserName)"
	Set-CobaltCheckSSL $global:_DefaultCobaltConnection.CheckSSL -Verbose:([bool]$PSBoundParameters['Verbose'].IsPresent)
	[Console]::Title = "Cobalt $($global:_DefaultCobaltConnection.Url) ($($global:_DefaultCobaltConnection.UserName))"
}

Function Write-CobaltConnection {
[CmdletBinding()]
param (
    [Parameter(Position=0, HelpMessage="The URL for the OData endpoint")][string]$Url = (Get-DefaultCobaltUrl),
    [Parameter(Position=1, HelpMessage="The credential to authenticate the OData call with")][PSCredential]$Credential = (Get-DefaultCobaltCredential),
	[Parameter(Mandatory=$True, HelpMessage="The name of the file to save the connection information in")][string]$FilePath,
    [Parameter(HelpMessage="Flag indicating whether to check SSL certificates when connecting to OData service, `$True = check certs, `$False = ignore untrusted certs")][Switch]$CheckSSL
)
	@{
		'Url'=$Url;
		'UserName'=$Credential.UserName;
		'Password'= ($Credential.Password | ConvertFrom-SecureString)
		'CheckSSL'=[bool]$CheckSSL
	} | ConvertTo-JSON | Out-File $FilePath
}

Function Initialize-DefaultConnection {
	$global:_DefaultCobaltConnection = New-Object -TypeName PSCustomObject -Property @{
		'Url'=$null;
		'UserName'=$null;
		'Password'=$null;
		'CheckSSL'=$null
	}
}

Function Get-DefaultCobaltUrl {
    if($global:_DefaultCobaltConnection -eq $null -or $global:_DefaultCobaltConnection.Url -eq $null) {
        throw "No default URL defined for Cobalt OData access. Use Set-DefaultCobaltConnection."
    }
    $global:_DefaultCobaltConnection.Url
}

Function Get-DefaultCobaltCredential {
    if($global:_DefaultCobaltConnection -eq $null) {
        Initialize-DefaultConnection
    }
    if($global:_DefaultCobaltConnection.UserName -eq $null) {
		$credential = Get-Credential
        $global:_DefaultCobaltConnection.UserName = $credential.UserName
        $global:_DefaultCobaltConnection.Password = $credential.Password
    }
    New-Object -TypeName PSCredential ($global:_DefaultCobaltConnection.UserName, $global:_DefaultCobaltConnection.Password)
}

Function Set-CobaltCheckSSL {
[CmdletBinding()]
param(
    [Parameter(Mandatory=$True,HelpMessage="Flag indicating whether to check SSL certificates when connecting to OData service, `$True = check certs, `$False = ignore untrusted certs")][Boolean]$Flag
)
	Write-Verbose "Setting Check SSL flag to $Flag"

	if ($Flag -eq $False) {
        add-type @"
            using System.Net;
            using System.Security.Cryptography.X509Certificates;
            public class TrustAllCertsPolicy : ICertificatePolicy {
                public bool CheckValidationResult(
                ServicePoint srvPoint, X509Certificate certificate,
                WebRequest request, int certificateProblem) {
                    return true;
                }
            }
"@
        [System.Net.ServicePointManager]::CertificatePolicy = New-Object TrustAllCertsPolicy
    }
    else {
        # Not entirely sure this works
        [System.Net.ServicePointManager]::CertificatePolicy = $null
    }
}

Function Get-CobaltQuery
{
[CmdletBinding()]
Param(
	[Parameter(HelpMessage="The URL for the OData operation")][string]$Url = (Get-DefaultCobaltUrl),
	[Parameter(HelpMessage="The credential to authenticate the OData call with")][PSCredential]$Credential = (Get-DefaultCobaltCredential),
	[Parameter(Position=0, HelpMessage="The remaining path and query parameters to append to the URL")][string]$Path
)
	if($Path.StartsWith('/')){
		$Path.Remove(0);
	}
	Write-Verbose "$Url/$Path"
    Invoke-RestMethod -Uri "$Url/$Path" -Credential $Credential -Headers @{'Authorization'=$(Convert-CredentialToAuthHeader -Credential $Credential)}
}

Function Get-CobaltEntity
{
[CmdletBinding()]
Param(
    [Parameter(HelpMessage="The URL for the OData endpoint")][string]$Url = (Get-DefaultCobaltUrl),
    [Parameter(HelpMessage="The credential to authenticate the OData call with")][PSCredential]$Credential = (Get-DefaultCobaltCredential),
    [Alias('entryUUID')][Parameter(Position=0, Mandatory=$True, HelpMessage="The UUID of the OData entity to get", ValueFromPipelineByPropertyName=$True)][System.Guid]$UUID,
	[Parameter(HelpMessage="A switch indicating to return the superior link information")][Switch]$Superior,
    [Parameter(HelpMessage="The type of OData entity to retrieve")][string]$ODataType
)
    $Path = "/topSet($UUID)"
    if(-not [string]::IsNullOrEmpty($ODataType)) {
        $Path += "/$ODataType"
    }
	if($Superior) {
		$Path += "?`$expand=superior"
	}
	Get-CobaltQuery -Url $Url -Path $Path -Credential $Credential -Verbose:([bool]$PSBoundParameters['Verbose'].IsPresent)
}

Function Get-CobaltEntities {
[CmdletBinding()]
Param(
    [Parameter(HelpMessage="The URL for the OData endpoint")][string]$Url = (Get-DefaultCobaltUrl),
    [Parameter(HelpMessage="The credential to authenticate the OData call with")][PSCredential]$Credential = (Get-DefaultCobaltCredential),
    [Parameter(Position=0, HelpMessage="The UUID of the superior OData the children of which to return", ValueFromPipelineByPropertyName=$True)][System.Guid]$ParentUUID,
    [Parameter(HelpMessage="The type of OData entity to retrieve")][string]$ODataType = $null,
    [Parameter(HelpMessage="An OData filter string to qualify the returned entities")][string]$ODataFilter,
	[Parameter(HelpMessage="A switch indicating the superior linked entity should be returned as well")][Switch]$Superior
)
    $Path = "/topSet"
    if(-not [string]::IsNullOrEmpty($entryUUID)) {
        $Path += "($entryUUID)/cobalt.descendants()"
    }
    if(-not [string]::IsNullOrEmpty($ODataType)){
        if($ODataType[0] -eq '#') {
            $ODataType = $ODataType.Substring(1)
        }
        $Path += "/$ODataType"
    }
    if(-not [string]::IsNullOrEmpty($ODataFilter)) {
        $Path += "?`$filter=$ODataFilter"
    }
	if($Superior) {
		$Path += "?`$expand=superior"
	}
	(Get-CobaltQuery -Url $Url -Path $Path -Credential $Credential -Verbose:([bool]$PSBoundParameters['Verbose'].IsPresent)).value
}

Function Get-CobaltProperty
{
[CmdletBinding()]
Param(
    [Parameter(HelpMessage="The URL for the OData endpoint")][string]$Url = (Get-DefaultCobaltUrl),
    [Parameter(HelpMessage="The credential to authenticate the OData call with")][PSCredential]$Credential = (Get-DefaultCobaltCredential),
    [Parameter(Position=0, Mandatory=$True, HelpMessage="The UUID of the OData entity to get")][System.Guid]$UUID,
    [Parameter(Position=1, Mandatory=$True, HelpMessage="The name of the property to retrieve")][string]$PropertyName
)
	(Get-CobaltEntity -Url $Url -Credential $Credential -UUID $UUID -Verbose:([bool]$PSBoundParameters['Verbose'].IsPresent))."$PropertyName"
}

Function Set-CobaltProperty {
[CmdletBinding()]
Param(
    [Parameter(HelpMessage="The URL for the OData endpoint")][string]$Url = (Get-DefaultCobaltUrl),
    [Parameter(HelpMessage="The credential to authenticate the OData call with")][PSCredential]$Credential = (Get-DefaultCobaltCredential),
    [Alias('entryUUID')]
    [Parameter(Position=0, Mandatory=$True, HelpMessage="The entryUUID of the OData entity to modify", ValueFromPipelineByPropertyName=$True)][ValidateNotNullOrEmpty()][System.Guid]$UUID,
    [Parameter(ParameterSetName="SingleProperty", Mandatory=$True, HelpMessage="The name of the OData property to modify")][string]$PropertyName,
    [Parameter(ParameterSetName="SingleProperty", HelpMessage="The value to set the OData property to")]$PropertyValue,
    [Parameter(ParameterSetName="MultipleProperties", Mandatory=$True, HelpMessage="A hash map containing the name/value pairs to use as property names and values")]$Properties
)
    if([string]::IsNullOrEmpty($UUID)) { # ValidateNotNullOrEmpty does not seem to catch this case...
        Throw "UUID of entry to modify is null or empty"
    }
    if($PsCmdlet.ParameterSetName -eq "SingleProperty"){
        $Properties = @{}
		$Properties.Add($PropertyName, $PropertyValue)
    }
	Write-Verbose ($Properties | ConvertTo-Json -Depth 3)
    $Properties | ConvertTo-Json -Depth 3 | Invoke-RestMethod -Uri "$Url/topSet($UUID)" -Method Patch -Credential $Credential -Headers @{'Authorization'=$(Convert-CredentialToAuthHeader -Credential $Credential)} -Verbose:([bool]$PSBoundParameters['Verbose'].IsPresent)
}

Function New-CobaltEntity {
[CmdletBinding()]
Param(
    [Parameter(HelpMessage="The URL for the OData endpoint")][string]$Url = (Get-DefaultCobaltUrl),
    [Parameter(HelpMessage="The credential to authenticate the OData call with")][PSCredential]$Credential = (Get-DefaultCobaltCredential),
    [Parameter(ParameterSetName="Hashtable", Mandatory=$True, HelpMessage="A hash map of property names and values to use for the new OData entity")][Hashtable]$Properties,
    [Parameter(ParameterSetName="Object", Mandatory=$True, HelpMessage="A PSCustomObject representing the new OData entity")][PSCustomObject]$Object,
    [Parameter(Mandatory=$True, HelpMessage="The OData type of the new entity")][string]$ODataType,
    [Parameter(HelpMessage="The UUID of the parent entity of the new entity")][System.Guid]$ParentUUID = $script:nullGUID
)

	$NewProperties = @{}

	if($ParentUUID -eq $script:nullGUID) {
		$ParentUUID = Get-CobaltOrganizationUUID -Url $Url -Credential $Credential
	}
	$NewProperties.Add('superior@odata.bind', "topSet($ParentUUID)")

	if(-not [string]::IsNullOrEmpty($ODataType)) {
		if($ODataType[0] -ne '#') {
			$ODataType = '#' + $ODataType
		}
		$NewProperties.Add('@odata.type', $ODataType)
	}

	if($PsCmdlet.ParameterSetName -eq 'Hashtable') {
		ForEach($p in $Properties.GetEnumerator()) {
			$NewProperties.Add($p.Name, $p.Value)
		}
	}
	elseif($PsCmdlet.ParameterSetName -eq 'Object') {
		ForEach($p in $Object.PSObject.Properties) {
			$NewProperties.Add($p.Name, $p.Value)
		}
	}
	Write-Verbose ($NewProperties | ConvertTo-JSON)
	($NewProperties | ConvertTo-Json | Invoke-RestMethod "$Url/topSet" -Credential $Credential -Method Post -Headers @{'Authorization'=$(Convert-CredentialToAuthHeader -Credential $Credential)} -Verbose:([bool]$PSBoundParameters['Verbose'].IsPresent))
    
}

Function Remove-CobaltEntity {
[CmdletBinding(SupportsShouldProcess=$true, ConfirmImpact="High")]
Param (
    [Parameter(HelpMessage="The URL for the OData endpoint")][string]$Url = (Get-DefaultCobaltUrl),
    [Parameter(HelpMessage="The credential to authenticate the OData call with")][PSCredential]$Credential = (Get-DefaultCobaltCredential),
    [Alias('entryUUID')][Parameter(Position=0,Mandatory=$True,HelpMessage="The UUID of the entity to delete",ValueFromPipelineByPropertyName=$True)][ValidateNotNullOrEmpty()][System.Guid]$UUID,
    [Parameter(HelpMessage="Deletes the specified entity along with all of its descendent entities. USE WITH CAUTION!")][Switch]$Recurse
)
	BEGIN {}
	PROCESS {
		if($PsCmdlet.ShouldProcess("$UUID")){
			if($Recurse -eq $False) {
				Invoke-RestMethod -Uri "$Url/topSet($UUID)" -Method Delete -Credential $Credential -Headers @{'Authorization'=$(Convert-CredentialToAuthHeader -Credential $Credential)} -Verbose:([bool]$PSBoundParameters['Verbose'].IsPresent)
			}
			else {
				# Delete recursievely not implemented
				Throw "Remove-CobaltEntity -Recurse not implemented"
			}
		}
	}
	END {}
}

# These two functions are generally useful for determining the superior entity UUID when creating new entities
Function Get-CobaltOrganizationUUID {
param(
    [Parameter(Position=0,HelpMessage="The URL for the OData endpoint")][string]$Url = (Get-DefaultCobaltUrl),
    [Parameter(Position=1,HelpMessage="The credential to authenticate the OData call with")][PSCredential]$Credential = (Get-DefaultCobaltCredential)
)
    (Get-CobaltQuery -Url $Url -Credential $Credential -Path '/topSet/com.viewds.cobalt.Organization?$select=entryUUID').value.entryUUID
}

Function Get-CobaltConfigurationUUID {
param(
    [Parameter(Position=0,HelpMessage="The URL for the OData endpoint")][string]$Url = (Get-DefaultCobaltUrl),
    [Parameter(Position=1,HelpMessage="The credential to authenticate the OData call with")][PSCredential]$Credential = (Get-DefaultCobaltCredential)
)
    (Get-CobaltQuery -Url $Url -Credential $Credential -Path '/topSet/com.viewds.cobalt.CobaltConfigurationContainer?$select=entryUUID').value.entryUUID
}

$script:DefaultScriptBlock = {param($entity, $depth) $entity}
[int]$script:currentDepth = 0

Function Get-CobaltSubtree {
[CmdletBinding()]
Param(
    [Parameter(HelpMessage="The URL for the OData endpoint")][string]$Url = (Get-DefaultCobaltUrl),
    [Parameter(HelpMessage="The credential to authenticate the OData call with")][PSCredential]$Credential = (Get-DefaultCobaltCredential),
    [Parameter(HelpMessage="The entity to start the tree traversal with")]$Root = (Get-CobaltEntities -Url $Url -Credential $Credential -ODataType 'cobalt.Organization' | Select -First 1),
    [Parameter(HelpMessage="A script block containing code to execute for each entity traversed in the configuration")][ScriptBlock]$Block = $script:DefaultScriptBlock,
    [Parameter(HelpMessage="Switch indicating the tree traversal should be depth first instead of breadth first")][Switch]$DepthFirst
)
	_Get-CobaltSubtree -Url $Url -Credential $Credential -Root $Root -Block $Block -DepthFirst:$DepthFirst -Depth 0 -Verbose:([bool]$PSBoundParameters['Verbose'].IsPresent)
}

Function _Get-CobaltSubtree {
[CmdletBinding()]
Param(
    [Parameter(Mandatory=$True)][string]$Url,
    [Parameter(Mandatory=$True)][PSCredential]$Credential,
    [Parameter(Mandatory=$True)]$Root,
    [Parameter(Mandatory=$True)][ScriptBlock]$Block = $script:DefaultScriptBlock,
    [Parameter(Mandatory=$True)][Switch]$DepthFirst,
	[Parameter(Mandatory=$True)][int]$Depth
)
    if($DepthFirst) {
		(Get-CobaltQuery -Url $Url -Credential $Credential -Path "/topSet($($Root.entryUUID))/cobalt.subordinates()" -Verbose:([bool]$PSBoundParameters['Verbose'].IsPresent)).value | %{
            _Get-CobaltSubtree -Url $Url -Credential $Credential -Root $_ -Block $Block -DepthFirst:$DepthFirst -Depth ($Depth + 1) -Verbose:([bool]$PSBoundParameters['Verbose'].IsPresent)
        }
        & $Block $Root $Depth
    }
    else {
        & $Block $Root $Depth
		(Get-CobaltQuery -Url $Url -Credential $Credential -Path "/topSet($($Root.entryUUID))/cobalt.subordinates()" -Verbose:([bool]$PSBoundParameters['Verbose'].IsPresent)).value | %{
            _Get-CobaltSubtree -Url $Url -Credential $Credential -Root $_ -Block $Block -DepthFirst:$DepthFirst -Depth ($Depth + 1) -Verbose:([bool]$PSBoundParameters['Verbose'].IsPresent)
        }
    }
}

Function Convert-CredentialToAuthHeader {
[CmdletBinding()]
Param(
    [Parameter(HelpMessage="The PSCredential object to use convert to a basic authentication Authorization header string")][PSCredential]$Credential
)
    $authHeader = "Basic {0}:{1}" -f ($Credential.GetNetworkCredential().username),($Credential.GetNetworkCredential().password)
    [Convert]::ToBase64String([Text.Encoding]::ASCII.GetBytes($authHeader))
}

# Useful filter function when generating custom properties via Select-Object, and the resulting property should be an array, but Select-Object screws it up
# See Michael Klement's answers at https://powershell.org/forums/topic/convertto-json-adds-count-and-value-property-names/ and 
# http://stackoverflow.com/questions/20848507/why-does-powershell-give-different-result-in-one-liner-than-two-liner-when-conve/38212718#38212718

filter Convert-ArrayProperties {
  # Loop over all properties of the input object at hand...
  foreach ($prop in (Get-Member -InputObject $_ -Type Properties)) {
    # ... and, for array-typed properties, simply reassign the existing 
    # property value via @(...), the array subexpression operator, which makes
    # such properties behave like regular collections again.
    if (($val = $_.$($prop.Name)) -is [array]) {
      $_.$($prop.Name) = @($val)
    }
  }
  # Pass out the (potentially) modified object.
  $_
}
#pragma warning(disable:4244)
#include <cpprest/http_client.h>
#include <cpprest/filestream.h>
#include <cpprest/interopstream.h>
#include <cpprest/asyncrt_utils.h>
#include <vector>
#include <iostream>

using namespace web;                        // Common features like URIs.
using namespace web::http;                  // Common HTTP functionality
using namespace web::http::client;          // HTTP client features
using namespace concurrency::streams;       // Asynchronous streams
using namespace utility;                    // Common utilities like string conversions
using namespace conversions;				// String conversions for utility::string_t

// This program demonstrates using the cross-platform Casablanca REST class library to access the Cobalt OData endpoint 
// using basic credentials. See https://github.com/Microsoft/cpprestsdk for information, including source code, about Casablanca
// See http://www.odata.org/ for more information about OData.

int main(int argc, char* argv[])
{
	// Be sure to change the username, password, and base OData URL before trying to run this code.
	static const char_t* username = U("<username>"); // Cobalt username that has access to OData
	static const char_t* password = U("<password>"); // password to use with username
	static const char_t* baseURL = U("<base OData URL"); // e.g. http://foo.bar.com:8080/baztenant/odata

	stdio_ostream<char_t> outStream = stdio_ostream<char_t>(std::wcout); // console stream with async support

	// Set the credentials in the client config N.B. we also have to set the Authorization header for HTTP Basic 
	// authentication
	http_client_config config;
	config.set_credentials(credentials(username, password));

	// Set up the request
	http_request request;

	// Create the authorization header string and Base-64 encode it
	string_t authHeader = string_t(U("Basic ")) + username + string_t(U(":")) + password;
	// N.B. this will generate a warning for potential loss of data due to conversion from char_t to unsigned char. You can ignore it.
	authHeader = to_base64(std::vector<unsigned char>(authHeader.begin(), authHeader.end()));
	request.headers().add(U("Authorization"), authHeader);

	// Specify the OData query
	request.set_request_uri(U("/topSet"));

	// Create http_client to send the request.
	http_client client(baseURL, config);

	// Create the request task, and follow it up with code to receive and print out the results
	pplx::task<void> requestTask = client.request(request).then([](http_response response)
		// Receive the response
		{
			return response;
		})

		// Handle response headers arriving.
		.then([&](http_response response)
		{
			outStream.print(U("Received response status code: "));
			outStream.print(response.status_code());
			outStream.print(U("\n"));
			return response;
		})

		// Extract the body of the response as a string
		.then([](http_response response)
		{
			return response.extract_string(true);
		})

		// And print it out
		.then([&](string_t json)
		{
			outStream.print(json);
		});

	// Invoke the request and wait until the entire response is received
	try
	{
		requestTask.get();
	}
	catch (const std::exception &e)
	{
		outStream.print(U("Error exception:"));
		outStream.print(e.what());
		outStream.print(U("\n"));
	}

	return 0;
}

